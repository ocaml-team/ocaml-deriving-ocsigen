Source: ocaml-deriving-ocsigen
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
  Stéphane Glondu <glondu@debian.org>
Build-Depends:
  ocaml-nox (>= 4.01),
  ocaml-findlib (>= 1.4),
  dh-ocaml (>= 0.9~),
  debhelper (>= 9),
  libtype-conv-camlp4-dev (>= 108),
  liboptcomp-camlp4-dev,
  oasis (>= 0.4.5),
  camlp4-extra
Standards-Version: 3.9.8
Homepage: https://github.com/ocsigen/deriving
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-deriving-ocsigen.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-deriving-ocsigen

Package: libderiving-ocsigen-ocaml-dev
Architecture: any
Depends:
 ${shlibs:Depends},
 ${ocaml:Depends},
 ${misc:Depends}
Conflicts: libderiving-ocaml, libderiving-ocaml-dev
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: deriving functions from type declarations in OCaml (devt files)
 Camlp4 extension to OCaml for deriving functions from type declarations.
 Includes derivers for pretty-printing, type-safe marshalling with
 structure-sharing, dynamic typing, equality, and more.
 .
 This is the version maintained by the Ocsigen project.

Package: libderiving-ocsigen-ocaml
Architecture: any
Depends:
 ${shlibs:Depends},
 ${ocaml:Depends},
 ${misc:Depends}
Conflicts: libderiving-ocaml, libderiving-ocaml-dev
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: deriving functions from type declarations in OCaml (runtime)
 Camlp4 extension to OCaml for deriving functions from type declarations.
 Includes derivers for pretty-printing, type-safe marshalling with
 structure-sharing, dynamic typing, equality, and more.
 .
 This is the version maintained by the Ocsigen project.
 .
 This package contains the shared runtime libraries.
